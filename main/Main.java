package main;
import main.DynamicPoints;
import java.util.Scanner;
import tinshoes.input.SafeScanner;
import tinshoes.geom.Point;
import tinshoes.geom.Line;
public class Main {
	public static void main(String[] args) {
		Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
		DynamicPoints dp = null;
		String errText = " is not valid input.";
		try {
			if (sc.yn("Do you want to specify a line verbosely?", "That is not valid input.")) {
				System.out.println("Generating line...");
				dp = new DynamicPoints(new Line(new Point(sc.nextDouble("X: ", errText, true), sc.nextDouble("Y: ", errText, true)), new Point(sc.nextDouble("X: ", errText, true), sc.nextDouble("Y: ", errText, true))));
			} else {
				double input = sc.nextDouble("What should the x-value of the vertical line be?", errText, true);
				dp = new DynamicPoints(new Line(new Point(input, 1), new Point(input, -1)));
			}
		} catch (Exception e) {
			System.err.println("An exception occurred: " + e.getMessage());
			return;
		}
		System.out.println(dp + "\nIntersection: " + dp.intersection());
	}
}