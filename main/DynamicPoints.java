package main;
import tinshoes.geom.Point;
import tinshoes.geom.Line;
public class DynamicPoints {
	private Line l, k;
	private double slope;
	public DynamicPoints(Line l) {
		this.l = l;
		if (l.slope() != (double)-1/(double)0)
			throw new IllegalArgumentException("The line is not vertical.");
		this.calcSlope(5);
		k = new Line (new Point(0, 0), new Point (this.slope, 1));
	}
	public void calcSlope(double limit) {
		this.slope = Math.random() * limit * (Math.random() > 0.5 ? -1 : 1);
	}
	public Point intersection() {
		return new Point(l.getPoint1().x, l.getPoint1().x * this.slope);
	}
	public Line getInput() {
		return this.l;
	}
	public Line getGeneratedLine() {
		return this.k;
	}
	public double getSlope() {
		return this.slope;
	}
	public String toString() {
		return "Vertical Line: " + l + "\nCurrent Line: " + k + "\nm = " + this.slope;
	}
}